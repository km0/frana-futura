# Frana Futura

![Landslide as a timeline](public/assets/img/landslide-as-timeline.png)

Frana Futura is a documentary that relates the Ligurian landslide condition and the realities that inhabit it, studying past evolutions in order to imagine possible future developments.

Liguria is a fragile and impervious territory in need of continuous maintenance. Neglect and soil consumption, depopulation and conversion into holiday homes, encourage abandonment and accelerate the crumbling of slopes - when they do not become luxurious fortresses.

The landslide is something that has always impregnated this territory, it is its geological and ontological conformation. It is an intrusive event that disrupts thinking as this usually think. It is a constant exception, and in its transformations it forces realities to reshape themselves in order to maintain its grip. Certain forces push downstream and others work to counteract them.

The interface element, for all, is the stone.

This documentary is a project by Sofia Merelli, Elena Bongiorno and me, with the collaboration of Micalis Papadopoulus for the sound design.

![Worlds of the walls](public/assets/img/dry-stone-walls.png)

Following there's an overview of the movie's chapters.

## Dry-stone walls

Dry-stone walls are a teeming with life and reality. Crafts that are handed down through transversal routes, from mothers and fathers and the internet, a heritage that is studied, forgotten and recovered. A dry stone wall is not just the sum of its stones.

## Slate quarries

Carlo, Paolo and Angelo work in the quarries of the F.lli Demartini company
in the Fontanabuona Valley.

Humans work their way through the darkness, between one seam of black slate and another. In the geological formations from which the landslide springs there are now underground tunnels and cathedrals. Back and forth from the belly of the mountains, incessant pouring of material. The forests are a carpet of waste.

## Pietre Parlanti

The old paths, the old mills, the terraces destined to agriculture, all covered by an inhospitable blanket of brambles, ivy, vitalba. A past crumbling along with an ignorant future.
Pietre Parlanti are a non-profit organisation based in Lavagna. By dint of brush cutters and chainsaws, the ancient paths, the canals that directed the waters, the pierced stones to support the vines, slowly re-emerge.

## Rockfall nets

The geologists Andrea Benedettini and Vittorio Bonaria work in the Ligurian region and supervise operations to secure and consolidate landslide slopes.
Steep, impervious, unstable. Metal nets completely bridle some areas, to contain the desire of these rocks to roll and scramble. Wire meshes that freeze the land.

![Cleaning the paths](public/assets/img/pulizia.jpg)
![Slate quarries](public/assets/img/cave.jpg)
![Remains of old cave works](public/assets/img/ciappaia.jpg)
![Water canalization](public/assets/img/arc.jpg)
![Tunnel](public/assets/img/tunnel.jpg)
